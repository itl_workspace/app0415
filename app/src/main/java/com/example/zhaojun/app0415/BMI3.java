package com.example.zhaojun.app0415;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class BMI3 extends ActionBarActivity {
    EditText et1, et2;
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi3);
        et1 = (EditText) findViewById(R.id.height);
        et2 = (EditText) findViewById(R.id.weight);
        tv1 = (TextView) findViewById(R.id.tv1);
    }

    public void cal(View v) {

        double height = Double.parseDouble(et1.getText().toString()) / 100;
        double weight = Double.parseDouble(et2.getText().toString());


        double result = weight / (height * height);
        tv1.setText(String.valueOf(result));

    }
}
